'use strict';

var ExamplePO = require("./ExamplePO");

describe("The example", function () {

    var po = new ExamplePO();
    
    beforeEach(function () {
        var mock = function () {
            var module = angular.module('mock', ['ngMockE2E']);
            module.run(function ($httpBackend) {
                $httpBackend.whenGET('/api/items').respond(['a','b', 'c', 'd']);
            });
        };
        browser.addMockModule('mock', mock);
        browser.get('/index.html');
    });

    it("should show my items", function () {
        expect(po.numberOfItems()).toBe(4);
    });

});
