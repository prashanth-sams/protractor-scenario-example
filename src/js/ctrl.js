(function() {
    'use strict';
    
    function Ctrl(service) {
        var vm = this;
        service.query({},function(data) {
            vm.items = data;
        });
        //vm.items = ['a', 'b', 'c'];
    }

    Ctrl.$inject = ['service'];
    angular.module('example').controller('ctrl', Ctrl);

})();